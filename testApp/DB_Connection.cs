﻿using System.Data.SQLite;
using System.IO;


namespace testApp
{
    class DB_Connection
    {
        public SQLiteConnection myConnection;
        public DB_Connection()
        {
            myConnection = new SQLiteConnection("Data Source=test.sqlite3");

            if (!File.Exists("./test.sqlite3"))

            {
                SQLiteConnection.CreateFile("test.sqlite3");
                System.Console.WriteLine("Database file created");
            }
        }

        public void OpenConnection()
        {
            if (myConnection.State != System.Data.ConnectionState.Open)
            {
                myConnection.Open();
            }
        }

        public void CloseConnection()
        {
            if (myConnection.State != System.Data.ConnectionState.Closed)
            {
                myConnection.Close();
            }
        }
    }
}

